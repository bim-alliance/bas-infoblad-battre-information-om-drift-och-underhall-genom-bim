![Bild 1](media/1.jpeg)

**Den information som genom BIM finns lagrad i byggprocessen bör komma förvaltaren till del.**

# Bättre information om drift och underhåll genom BIM

> #### Ett nyligen avslutat projekt om BIM i förvaltarskedet visar att en 3D-modell endast behöver geometrier, läge och ID för att fungera i ett förvaltarperspektiv. Den övriga informationen kan hämtas från tabellformat som länkas till modellen. Beställare uppmanas att ställa krav på BIM i byggprocessen för att därigenom lättare få fram användbar information till drift och underhåll.

DEN INFORMATION SOM IDAG FINNS LAGRAD i byggprocessen kommer sällan förvaltaren till del. Samtidigt ställer beställare inga eller alltför få krav på BIM. För att aktualisera hur man kan ha nytta av BIM i drift- och förvaltningsskedet har NCC initierat projektet ”BIM i förvaltningsskedet för små och medelstora projekt”.
​	– Vi ville undersöka på vilket sätt information kan ges till
en förvaltare i en BIM-miljö efter att byggprojektet är färdigt. Våra beställare måste bli mer kunniga i BIM, ställa krav och ta tag i frågan om hur information ska föras vidare, säger Marcus Bergljung, NCC, som tillsammans med Andréas Ask, NCC, lett projektet.
​	I de riktigt stora byggprojekten är det nödvändigt att hantera den stora informationsmängden och där finns det ekonomiska resurser att låta någon jobba med detta. Men i små och medelstora projekt är läget annorlunda. Därför har fokus i projektet legat på låg kostnad och enkelhet. 
​	Projektet har bestått av två delar. I den ena delen har man ur ett förvaltningsperspektiv undersökt var man, bland de olika lagringsplatser för information som finns i en byggnads livslängd, lämpligast placerar informationen. Den andra delen har behandlat hur entreprenören på bästa sätt kan leverera dagens drift- och underhållsinformation i en digital miljö. Idag är det låg nivå på överflyttning av informationen, menar Marcus Bergljung. Målet har varit att man som förvaltare ska kunna navigera i 3D-modellen och där hitta DoU-dokument som är länkade till de delar i byggnaden som de tillhör. 
​	– Om man inte har tydliga krav från början blir kostnaderna stora men om man från början beställer vad man vill ha så får man det. Vissa beställare vill ha allt men de måste välja.

BIM inbegriper all information i bygg- och förvaltningsprocessen, från CAD-modeller till kalkyler och excelldokument. Det finns hundratals lagringsplatser och dessutom information som inte alltid ska vara tillgänglig för alla. Många vill få in all information i CAD-modellen, men det är ingen bra väg att gå. Det är snarare bra att inte ha för mycket information här. Förvaltarna vill inte behöva anlita CAD-operatörer så fort som de ska ändra något. Det är betydligt enklare att administrera, flytta och uppdatera data i sina egna system som är länkade till modellen. 

![Bild 2](media/2.jpeg)

**All information behöver inte ligga i 3D-modellerna. Den tabellerade**
**informationen kan med fördel vara utanför.** 

Den minsta informationsnivån i en CAD-modell är ID, läge och geometriska storheter som längd, bredd och höjd. Då kan man koppla ihop 3D-modellen med all form av information som har samma ID-nummer. 
​	– All tabellerad information kan med fördel vara utanför CAD-modellen, säger Marcus Bergljung. Det blir mycket enklare att istället koppla dem till varandra.

DET FINNS FLERA OLIKA PROGRAMVAROR, med olika funktioner, egenskaper och licenshantering, som kan länka information till 3D-modeller. I projektet har fyra programvaror testats för att användas till en digital DoU-pärm. Istället för att slå i en pärm letar man exempelvis upp ett fönster i 3D-modellen och får smidigt all information som är kopplad till just det fönstret. Av programvarorna visade sig Navisworks Simulate/Manage/Freedom fungera smidigt och effektivt och totalt sett bäst. Programmet har testats i NCC Boende-projektet Beckomberga i  Stockholm. Även programmet Artra testades. Dess applikationer är exempel på ett mer komplext kopplingsverktyg. Artra fungerar för sitt ändamål men det krävs mycket för att sätta upp systemet.

​	– Även om originalprogrammen fungerar bra så är de inte så säkra på lång sikt när utbudet av programvaror minskar. Istället lutar jag åt att IFC-formatet blir bäst. Standarden är långsiktigt bra och förbättras över tid. Om vi fokuserar på IDn och objekten så behöver vi inte tänka så mycket på att informationen ska hamna rätt i IFC-exporten. Vi som entreprenörer vill gärna jobba med IFC.

ATT ANVÄNDA BIM TILL DOU INNEBÄR stora fördelar. Man förlorar inte information under processens gång och med 3D-modeller  blir det mycket lättare att komma åt den. Uppgifterna blir även mer exakta och beständiga.
​	– Man mäter mycket i fastighetsbranschen, om och om igen. Dessutom mäter man olika och får olika resultat. Alla dessa mätningar, som kostar mycket pengar, slipper man med BIM. Egentligen är det inga större svårigheter att använda BIM
till drift och underhåll. Det mesta av informationen finns men det gäller att göra den lättillgänglig och visualiserbar. Man kan till exempel snabbt få en övergripande bild av ett helt våningsplan i en fastighet, säger Marcus Bergljung.
​	Projektet är avslutat men arbetet med de här frågorna fortsätter i OpenBIMs förvaltningsgrupp. En sidogrupp till denna, som framför allt består av förvaltare, diskuterar här vilka vägval man ska göra, hur man tydligt ställer krav, hur informationen ska presenteras och vilken information som förvaltarna vill ha.

